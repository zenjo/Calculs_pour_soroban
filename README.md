# Calculs pour soroban
## Qu’est que le soroban ?

Le soroban est un abaque, plus précisément le boulier compteur japonais. Le soroban mène à l’anzan, c-à-d au calcul mental sur base d’une représentation mentale du soroban. Soroban et anzan sont redoutablement efficaces et permettent avec de l’entraînement d’atteindre des vitesses impressionnantes. Pour les même calculs, les personnes entraînées dépasseront nettement la vitesse d’une personne équipée d’une calculatrice électronique. 

Au japon, le soroban est un sport ou un art qui est enseigné dans de nombreuses écoles spécialisées, et donne lieu à des concours. 

Ce logiciel a été construit à la fois pour pouvoir produire de grandes feuilles avec de nombreux calculs sur ordinateur, et néanmoins être convivial avec les smartphones et les tablettes

## Vous doutez de l'efficacité du soroban ?
Je vous propose ces vidéos:

* [Soroban - All in the mind](http://www.youtube.com/watch?v=Px_hvzYS3_Y);
* [Soroban japonais démonstration](http://www.youtube.com/watch?v=lpg_UEvocE4).


## Voir
* la démo, c'est par [ici](http://soroban.sebille.name/);
* les captures d'écran, c'est par [là](https://gitlab.adullact.net/zenjo/Calculs_pour_soroban/wikis/home/captures-d'%C3%A9cran);
* et [quelques liens intéressants](https://gitlab.adullact.net/zenjo/Calculs_pour_soroban/wikis/home) à propos du soroban.

## Installation
Calculs pour soroban est un logiciel web. Son installation nécessite un serveur web et un serveur php; pas de base de données.

## Langues
Le logiciel est en français et traduit en anglais. Si quelqu'un veut traduire dans une autre langue (ou vérifier ma traduction anglaise - l'anglais n'est pas ma langue maternelle), il est le bienvenu.

Dans le répertoire [translation_files](https://gitlab.adullact.net/zenjo/Calculs_pour_soroban/tree/master/translation_files), il y a 2 fichiers avec les termes à traduire : soroban_calculations_translation_2.3.ods et soroban_calculations_translation_2.3.xls; les 2 fichiers sont les mêmes au format open document ou Microsoft excel - à votre préférence, un seul suffit donc à la traduction. Une fois celle-ci faite, je me charge de l'intégration; votre nom est repris si vous le souhaitez.
