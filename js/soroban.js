// (C) Robert Sebille 2014 http://sebille.name This script is under License GNU GPL V3. See LICENCE.

$(document).ready(function(){
    
    var version="2.4";

    var hs=0  // hide speed
    var ss=0  // show speed
    var typeop=""
    var mindigit=0
    var maxdigit=0
    var numnumb=0
    var numprob=0
    var mulandquot=0
    var mulordivor=0
    var mulandquotstr=""
    var mulordivorstr=""
    var t_multiplicande=""
    var t_multiplicateur=""
    var t_quotient=""
    var t_diviseur=""

/*    $.ajaxSetup ({
        // Disable caching of AJAX responses
        cache: false
    });
*/

    /**************************/
    /* Fonctions pour cookies */
    /**************************/
    // From http://fr.openclassrooms.com
    function setCookie(sName, sValue) {
        var today = new Date(), expires = new Date();
        expires.setTime(today.getTime() + (365*24*60*60*1000));
        document.cookie = sName + "=" + encodeURIComponent(sValue) + ";expires=" + expires.toGMTString();
    }

    function getCookie(sName) {
        var oRegex = new RegExp("(?:; )?" + sName + "=([^;]*);?");
 
        if (oRegex.test(document.cookie)) {
            return decodeURIComponent(RegExp["$1"]);
        } else {
            setCookie("langue", "EN")
            return "EN";
        }
    }
    
    $("div.avert").hide();
    if (navigator.cookieEnabled) {$("div#t_cookies").hide();} else {$("div#t_cookies").show(); $("div#avert_bouton").show(ss);}
    
    /*********************/
    /* Fonctions langues */
    /*********************/
    function setLangue(pays) {
        switch(pays) {
            case "FR":
                // Messages statiques
                $("span.t_Soroban_calculations").html("Calculs pour soroban");
                $("div#responsive").html("Site web adaptatif");
                $("span.t_help").html("Aide");
                $("span.t_Min_number_of_digits").html("Nombre min. de chiffres");
                $("span.t_Max_number_of_digits").html("Nombre max. de chiffres");
                $("span.t_Number_of_numbers").html("Nombre de nombres");
                $("span.t_Number_of_problems").html("Nombre de probl&egrave;mes");
                $("span.t_Number_of_digits").html("nombre de chiffres");
                $("span.t_Get_new_calculations").html("Nouveaux probl&egrave;mes");
                $("span.t_Answers").html("R&eacute;ponses");
                $("span.t_The_calculations_will_come_here").html("Zone d'affichage des probl&egrave;mes");
                $("span.t_Hide_title_settings").html("Masquer titre & param. O/N");
                $("span.t_Usefull_to_print_a_sheet").html("Convivial avec les samrtphones et les tablettes<br />quand vous choisissez un petit \"Nombre de probl&egrave;mes\".");
                $("div#t_sys32bits").html("Le système qui exécute le générateur de nombres aléatoires ne supporte pas les entiers plus grands que 2.147.483.647. Je dois diminuer la grandeur des problèmes disponibles.");
                $("div#t_cookies").html("Les cookies sont désactivés. Je ne pourrai pas retenir vos préférences de langue.");
                $("button#hide_warnings").text("Cacher les avertissements");
                // Messages dynamiques
                t_multiplicande="Multiplicande";
                t_multiplicateur="Multiplicateur";
                t_quotient="Quotient";
                t_diviseur="Diviseur";
             break;
            case "EN":
                // Messages statiques
                $("span.t_Soroban_calculations").html("Soroban calculations");
                $("div#responsive").html("Responsive web design");
                $("span.t_help").html("Help");
                $("span.t_Min_number_of_digits").html("Min. number of digits");
                $("span.t_Max_number_of_digits").html("Max. number of digits");
                $("span.t_Number_of_numbers").html("Number of numbers");
                $("span.t_Number_of_problems").html("Number of problems");
                $("span.t_Number_of_digits").html("number of digits");
                $("span.t_Get_new_calculations").html("Get new calculations");
                $("span.t_Answers").html("Answers");
                $("span.t_The_calculations_will_come_here").html("The calculations will come here");
                $("span.t_Hide_title_settings").html("Hide title & settings Y/N");
                $("span.t_Usefull_to_print_a_sheet").html("User-friendly with samrtphones and tablets when<br />you choose a low \"Number of problems\".");
                $("div#t_sys32bits").html("The system which runs the random numbers generator does not support integers greater than 2,147,483,647. I must decrease the size of the problems available.");
                $("div#t_cookies").html("Cookies disabled. I can not remember your language preference.");
                $("button#hide_warnings").text("Hide the warnings");
                // Messages dynamiques
                t_multiplicande="Multiplicand";
                t_multiplicateur="Multiplicator";
                t_quotient="Quotient";
                t_diviseur="Divisor";
            break;
            default:
        }
    }

    function setMessagesDynamiques() {
        // On clique sur un chqngement de langue. Aussi nécessaire avec les boutons x et /
        switch(typeop) {
            case "mul":
                $("span#mulandquotstr").html(t_multiplicande);
                $("span#mulordivorstr").html(t_multiplicateur);
            break;
            case "div":
                $("span#mulandquotstr").html(t_quotient);
                $("span#mulordivorstr").html(t_diviseur);
            break;
            default:
        }
        
    }
    var langue=getCookie("langue");
    setLangue(langue);
    
   // jQuery methods go here...
    $("div.param").hide();
    
    // 32 bits (nombres max de 9 chiffres pour mt_rand) or 64 ? Si 32, on affiche un avertissement.
    var sysbits=getCookie("sysbits");
    $("div#t_sys32bits").hide();
    $.get("ajax/check_randmax.php",function(result) {
        // result == "32" ou "64"
        if (sysbits == "32") {
            $("div#t_sys32bits").show(ss);
            $("div#avert_bouton").show(ss);
            $("div#addsub").load("ajax/addsubform32bits.html");
            $("div#muldiv").load("ajax/muldivform32bits.html");
            }
    });

    // Déterminer le chemin du fihier html courant
    var currentpath = $(location).attr('pathname');
    var filepath = "";
    if (currentpath.indexOf("help") > -1) {filepath = "../"};

    // Charge d'éventuels scripts de stat depuis ajax/scripts_stats.txt
    $("div#stats").load(filepath+"ajax/scripts_stats.txt");

    /***********************************************/
    /* Montre / cache les paramètres par opération */
    /***********************************************/
    // Général
    $("div#hideparamtool").hide();
    $("span#op").hide();
    
    $("button#plus, button#moins, button#plusoumoins").click(function(){
        $("div.param").hide(hs);
        $("div#addsub, div#getnew").show(ss);
        
    });

    $("button#fois, button#divise").click(function(){
        $("div.param").hide(hs);
        $("div#muldiv, div#getnew").show(ss);
    });

    // Correction du déplacement de la perle
    corrbead=-4;
    // Particulier aux oper
    $("button#plus").click(function(){
        typeop="add";
        $("button.bouton").css("border", "2px none");
        $("button#plus").css("border", "2px solid #999");
        x=$("button#plus").position();
        $("img#bead").animate({left:x.left+corrbead}, "slow");
        $("span#op").html("+");
    });
    
    $("button#moins").click(function(){
        typeop="sub";
        $("button.bouton").css("border", "2px none");
        $("button#moins").css("border", "2px solid #999");
        x=$("button#moins").position();
        $("img#bead").animate({left:x.left+corrbead}, "slow");
        $("span#op").html("-");
    });
    
    $("button#plusoumoins").click(function(){
        typeop="addsub";
        $("button.bouton").css("border", "2px none");
        $("button#plusoumoins").css("border", "2px solid #999");
        x=$("button#plusoumoins").position();
        $("img#bead").animate({left:x.left+corrbead}, "slow");
        $("span#op").html("&plusmn;");
    });
    
    $("button#fois").click(function(){
        typeop="mul";
        $("button.bouton").css("border", "2px none");
        $("button#fois").css("border", "2px solid #999");
        setMessagesDynamiques();
        x=$("button#fois").position();
        $("img#bead").animate({left:x.left+corrbead}, "slow");
        $("span#op").html("&times;");
    });
    
    $("button#divise").click(function(){
        typeop="div";
        $("button.bouton").css("border", "2px none");
        $("button#divise").css("border", "2px solid #999");
        setMessagesDynamiques();
        x=$("button#divise").position();
        $("img#bead").animate({left:x.left+corrbead}, "slow");
        $("span#op").html("&divide;");
    });


    /******************************************/
    /* On veut un nouveau calcul de solutions */
    /******************************************/
    $("button#new").click(function(){
        $("div#hideparamtool").show(ss);

        // Récupération des valeurs pour les requêtes ajax
        if (typeop == "add" || typeop == "sub" || typeop == "addsub") {
            mindigit=parseInt(document.addsous.mindigit.value);
            maxdigit=parseInt(document.addsous.maxdigit.value);
            if (mindigit > maxdigit) {
                swap=mindigit; mindigit=maxdigit; maxdigit=swap;
                document.addsous.mindigit.value=mindigit; document.addsous.maxdigit.value=maxdigit;
            }
            numnumb=parseInt(document.addsous.numnumb.value);
            numprob=parseInt(document.addsous.numprob.value);
 
        // Requête ajax
            $.ajax({url:"ajax/addsub.php", type:"POST", data:{TYPEOP:typeop,MINDIGIT:mindigit,MAXDIGIT:maxdigit,NUMNUMB:numnumb,NUMPROB:numprob}, success:function(result){
                $("table#resultat").html(result);
                if ($("input#answersyn").is(":checked")) {$("p.reponse").show(); $("p.noreponse").hide();}
                    else {$("p.reponse").hide(); $("p.noreponse").show();}
            }});
        }

        // Récupération des valeurs pour les requêtes ajax
        if (typeop == "mul" || typeop == "div") {
            mulandquot=parseInt(document.muldiv.mulandquot.value);
            mulordivor=parseInt(document.muldiv.mulordivor.value);
            numprob=parseInt(document.muldiv.numprob.value);

        // Requête ajax
            $.ajax({url:"ajax/muldiv.php", type:"POST", data:{TYPEOP:typeop,MULANDQUOT:mulandquot,MULORDIVOR:mulordivor,NUMPROB:numprob}, success:function(result){
                $("table#resultat").html(result);
                if ($("input#answersyn").is(":checked")) {$("p.reponse").show(); $("p.noreponse").hide();}
                    else {$("p.reponse").hide(); $("p.noreponse").show();}
            }});
//        $("table#resultat").html("Coming soon... ajax param: "+typeop+" "+mulandquot+" -- "+mulordivor+" --- "+numprob);
        }    
    });

    /************/
    /* Réponses */
    /************/
    $("input#answersyn").click(function(){
        $("p.noreponse").toggle(ss);
        $("p.reponse").toggle(ss);
    });

    /****************************/
    /* Print hide title & param */
    /****************************/
    $("button#hideparam").click(function(){
        $("H1").toggle(ss);
        $("div#perle").toggle(ss);
        $("div.boutons_operations").toggle(ss);
        if (typeop == "add" || typeop == "sub" || typeop == "addsub") {$("div#addsub").toggle(ss);}
        if (typeop == "mul" || typeop == "div") {$("div#muldiv").toggle(ss);}        
        $("div#infohideparam").toggle(ss);
        $("span#op").toggle(ss);
//        $("div#footer").toggle(ss);
    });

    /***********************/
    /* Gestion des langues */
    /***********************/
    $("button#lang_fr").click(function(){
        setCookie("langue", "FR")
        langue="FR";
        setLangue("FR");
        setMessagesDynamiques();
    });

    $("button#lang_en").click(function(){
        setCookie("langue", "EN")
        langue="EN";
        setLangue("EN");
        setMessagesDynamiques();
    });


    /*****************************/
    /* Cacher les avertissements */
    /*****************************/
    $("button#hide_warnings").click(function(){
        $("div.avert").hide();
    });

    /********************************/
    /* Afficher / cacher les QRcode */
    /********************************/
    $("button#qrcode_url_show").click(function(){
        $("div#qrcode_vcard").hide();
        $("div#qrcode_url").show();
        $("button#qrcode_hide").show();
    });

    $("button#qrcode_vcard_show").click(function(){
        $("div#qrcode_url").hide();
        $("div#qrcode_vcard").show();
        $("button#qrcode_hide").show();
    });

    $("button#qrcode_hide").click(function(){
        $("div#qrcode_url").hide();
        $("div#qrcode_vcard").hide();
        $("button#qrcode_hide").hide();
    });

    /**********************************************/
    /* Redirections vers des ressources diverses */
    /*********************************************/

    $("button#help").click(function(){
        switch(langue) {
            case "FR":
                window.location.assign("help/help_fr.html");
            break;
            case "EN":
                window.location.assign("help/help_en.html");
            break;
            default:
        }
    });
    
    $("button#learnsheet").click(function(){
        window.location.assign("http://www.uitti.net/stephen/soroban/soroban_sheets.pl");
    });
    
    $("button#prosheet").click(function(){
        window.location.assign("http://www.sorobanexam.org/generator.html");
    });

    $("button#tomoe").click(function(){
        window.location.assign("http://www.soroban.com/index_eng.html");
    });

    /**********/
    /* footer */
    /**********/
    var now = new Date();
    annee_courante = now.getFullYear();

    //$("div#footer").html("Soroban calculations V. - "+version+" <a href='https://www.gnu.org/licenses/gpl.html'>GNU GPL License V3</a><br /><a href='"+filepath+"help/translations.html'>Translations</a> - <a href='"+filepath+"images/'>Screenshots</a> - 3rd party <a href='https://jquery.org/license/'>jQuery license</a><br />&copy; <a href='http://sebille.name'>Robert Sebille</a> 2014 - "+annee_courante+" - <a href='http://web.sebille.name/nextcloud/index.php/s/ee4q3jnXQE4YF2R' target='_blank'>Get the source code</a>");
    $("div#footer").html("Soroban calculations V. - "+version+" <a href='https://www.gnu.org/licenses/gpl.html'>GNU GPL License V3</a><br /><a href='"+filepath+"help/translations.html'>Translations</a> - <a href='"+filepath+"images/'>Screenshots</a> - 3rd party <a href='https://jquery.org/license/'>jQuery license</a><br />&copy; <a href='http://sebille.name'>Robert Sebille</a> 2014 - "+annee_courante+" - <a href='https://gitlab.adullact.net/zenjo/Calculs_pour_soroban' target='_blank'>Get the source code</a>");


    

});
