<?php
// (C) Robert Sebille 2014 http://sebille.name This script is under License GNU GPL V3. See LICENCE.

include("fonctions.php");

// On récupère les valeurs passées en post
$typeop = $_POST["TYPEOP"];
$mindigit = $_POST["MINDIGIT"];
$maxdigit = $_POST["MAXDIGIT"];
$numnumb = $_POST["NUMNUMB"];
$numprob = $_POST["NUMPROB"];

// On détermine les nombres max et min
$maxnumber = pow(10,($maxdigit)) - 1;
$minnumber = pow(10,($mindigit-1));

$retour = "<tr>";
for ($i = 0; $i < $numprob; $i++) {
    // formater le tableau comme je veux
//    if ($i > 0 AND $numprob < 5) {$retour .= "</tr><tr>";}
    if ($i > 0 AND ($i % 5) == 0) {$retour .= "</tr><tr>";}

    $retour .= "<td class='nombre'>";
    $total = 0;
    for ($j = 1; $j <= $numnumb; $j++) {
        switch ($typeop) {
            case "add":
                $number = mt_rand($minnumber, $maxnumber);
                $total += $number;
                if ($j == $numnumb) {$retour .= "+ ".number_format($number, 0, ',', ' ' )."<br />";}
                    else {$retour .= number_format($number, 0, ',', ' ' )."<br />";}
                //$retour .= "+ ".number_format($number, 0, ',', ' ' )."<br />";
                break;
            case "sub":
                if ($j == 1) {$number = mt_rand($minnumber+floor(($maxnumber-$minnumber)/2), $maxnumber);$total += $number;}
                    else {
                        if ($total > $minnumber) {$number = mt_rand($minnumber, $total);$total -= $number;}
                            else {$number = 0;}
                    }
                
                if ($j > 1) {$retour .= "- ".number_format($number, 0, ',', ' ' )."<br />";}
                    else {$retour .= number_format($number, 0, ',', ' ' )."<br />";}
                //$retour .= "+ ".number_format($number, 0, ',', ' ' )."<br />";
                break;
            case "addsub":
                if ($j == 1) {$number = mt_rand($minnumber+floor(($maxnumber-$minnumber)/2), $maxnumber);$total += $number;}
                if ($j > 1) {
                    $oper = rand(0,1);
                    if ($oper == 0) {  // on dit qu'alors c'est add, sinon c'est sub
                        $number = mt_rand($minnumber, $maxnumber); $total += $number;}
                    else {
                        // Prévenir que le total soit plus grand que $maxnumber
                        if ($total > $maxnumber) {$tmp_maxnumber = $maxnumber;} else {$tmp_maxnumber = $total;}
                        if ($total > $minnumber) {$number = mt_rand($minnumber, $tmp_maxnumber);$total -= $number;}
                            else {$number = 0;}
                        }
                    }
                
                if ($j > 1 AND $oper == 1) {$retour .= "- ".number_format($number, 0, ',', ' ' )."<br />";}
                    else {$retour .= number_format($number, 0, ',', ' ' )."<br />";}
                //$retour .= "+ ".number_format($number, 0, ',', ' ' )."<br />";
                break;
        }
    }
    $retour .= "<p class='reponse'>".number_format($total, 0, ',', ' ' )."</p><p class='noreponse'>&nbsp;</p></td>";

}
$retour .= "</tr>";
echo $retour;


//    echo "Coming soon... ajax param: ".$typeop." ".$mindigit." - ".$maxdigit." -- ".$numnumb." --- ".$numprob." + ".$maxnumber." - ".$minnumber;


?>