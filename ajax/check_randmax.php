<?php

// On vérifie si le système supporte tous les nombres à 10 chiffres. Si oui, OS ou CPU 64 bits, sinon, OS ou CPU 32 bits
//$mtrandmax =  2147483648;

//$mtrandmax =  9999999999999999*999;
//$mtrandmax =  9999999999; // <- sautait l'exception! why??
$mtrandmax =  3333333333;

function mtr($mtrm) {
    if (!mt_rand(0, $mtrm)) {
        throw new Exception('32 bits.');
        return null;
    }
    else {
        return mt_rand(0, $mtrm);
    }
}

$sys32bits="0";
try {
    $test = mtr($mtrandmax);
} catch (Exception $e) {
    $sys32bits="1";
}

if ($sys32bits == "0") {setcookie("sysbits", "64", time()+3600, "/");} else {setcookie("sysbits", "32", time()+3600, "/");}

?>