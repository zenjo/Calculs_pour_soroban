<?php
// (C) Robert Sebille 2014 http://sebille.name This script is under License GNU GPL V3. See LICENCE.

include("fonctions.php");

// On récupère les valeurs passées en post
$typeop = $_POST["TYPEOP"];
$mulandquot = $_POST["MULANDQUOT"];
$mulordivor = $_POST["MULORDIVOR"];
$numprob = $_POST["NUMPROB"];

// On détermine les nombres max et min
$maxmqnumber = pow(10,($mulandquot)) - 1;
$minmqnumber = pow(10,($mulandquot-1));
$maxmdnumber = pow(10,($mulordivor)) - 1;
$minmdnumber = pow(10,($mulordivor-1));

$retour = "<tr>";
for ($i = 0; $i < $numprob; $i++) {
    // formater le tableau comme je veux
//    if ($i > 0 AND $numprob < 5) {$retour .= "</tr><tr>";}
    if ($i > 0 AND ($i % 5) == 0) {$retour .= "</tr><tr>";}

    $retour .= "<td class='nombre'>";
    $total = 0;
//    $total = $mulandquot * $mulordivor;
    $mqnumber = mt_rand($minmqnumber, $maxmqnumber);
    $mdnumber = mt_rand($minmdnumber, $maxmdnumber);
    $total = $mqnumber * $mdnumber;
        switch ($typeop) {
            case "mul":
                $retour .= number_format($mqnumber, 0, ',', ' ' )."<br />x ".number_format($mdnumber, 0, ',', ' ' );
                $retour .= "<p class='reponse'>".number_format($total, 0, ',', ' ' )."</p><p class='noreponse'>&nbsp;</p></td>";                break;
            case "div":
                $retour .= number_format($total, 0, ',', ' ' )."<br />&divide; ".number_format($mdnumber, 0, ',', ' ' );
                $retour .= "<p class='reponse'>".number_format($mqnumber, 0, ',', ' ' )."</p><p class='noreponse'>&nbsp;</p></td>";                break;
                break;
        }

}
$retour .= "</tr>";

//$retour = $maxmqnumber." - ".$minmqnumber." --- ".$maxmdnumber." - ".$minmdnumber." == ".$mqnumber." - ".$mdnumber;
echo $retour;


//    echo "Coming soon... ajax param: ".$typeop." ".$mindigit." - ".$maxdigit." -- ".$numnumb." --- ".$numprob." + ".$maxnumber." - ".$minnumber;


?>